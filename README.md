This repository is deprecated, please see https://gitlab.com/duobiblo/app2.

# Frontend for reading bibles in the browser

## Install
- clone this repository
- npm install
- npm run dev # dev

## Software Architecture

Frontend: 
- ES6, Sapper, Svelte
- All state is written to IndexedDB, WebSQL, or localStorage thanks to [localforage](https://github.com/localForage/localForage)
- Tests are written with Cypress 

Backend: 
- No server state
- Bibles are hosted as js files on GitLab Pages
- Bibles are scraped and transformed with Node.js scripts

Contributions
- Branch/Fork
- Add your changes, add unit and api tests for your new features
- Run all tests locally (npm run test)
- Submit a PR