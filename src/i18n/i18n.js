import { writable } from 'svelte/store'
import { uiLanguage } from '../stores/UILanguage.js'
import { getDefaultLanguage } from '../lib/helpers.js'
import en from './en.json'
import es from './es.json'
import de from './de.json'

const uiTranslations = {
    en,
    es,
    de,
}

let currentUILanguage = getDefaultLanguage()
const currentUITranslations = writable()

uiLanguage.subscribe($uiLanguage => {
    if ($uiLanguage) currentUILanguage = $uiLanguage
    currentUITranslations.set(uiTranslations[$uiLanguage])
})

const SUBKEY_DELIMITER = '.'

const getValue = key => {
    const $currentUITranslations = uiTranslations[currentUILanguage]
    let value = $currentUITranslations && $currentUITranslations[key]
    if (!value) {
        const subkeys = key.split(SUBKEY_DELIMITER)
        let candidate = $currentUITranslations
        if (candidate && subkeys.length > 1) {
            for (const subkey of subkeys) {
                candidate = candidate && candidate[subkey]
            }
            if (candidate) value = candidate
        }
    }
    return value
}

export const t = (key, ...args) => {
    let value = getValue(key)
    if (!value) {
        if (!currentUILanguage === 'en') console.warn(`UI translation key '${key}' for language '${currentUILanguage}' not found`)
        value = uiTranslations['en'][key] || key
    }
    args.forEach(arg => {
        for (const [paramName, paramValue] of Object.entries(arg)) {
            const regexp = new RegExp(`{{${paramName}}}`, 'g')
            value = value.replace(regexp, paramValue)
        }
    })
    return value
}