import localforage from 'localforage'
import { DEFAULTS, availableUILanguages, DUOBIBLO_BACKEND } from '../lib/params.js'
import { notifyError } from '../stores/NotificationStore.js'
import { compute_rest_props } from 'svelte/internal'

export const range = (to, n = 0) => Array(parseInt(to, 10)).fill(parseInt(n, 10)).map((x, y) => x + y)
export const rangeFromTo = (from, to) => {
    from = parseInt(from, 10)
    to = parseInt(to, 10)
    return Array(to - from + 1).fill(from).map((x, y) => x + y)
}
export const formatNumber = (n) => n ? parseInt(n, 10).toLocaleString() : '0'
export const sleep = (time) => new Promise((resolve) => setTimeout(resolve, time))
export const isMobile = () => /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)
export const isBrowser = () => typeof window !== 'undefined'
export const isProduction = () => isBrowser() && location.href.toLowerCase().includes('duobiblo')

// zoom back to normal screen size if zoomed
export const zoomOutMobile = () => {
    const viewport = document.querySelector('meta[name="viewport"]')
    if (isMobile() && viewport) {
        viewport.content = 'initial-scale=0.1'
    }
}
  
localforage.config({ name: DEFAULTS.PROJECT_NAME })

const readError = (error) => {
    console.error(error)
    notifyError('A value could not be read. You may use an unsuported device.')
}

export const readFromDevice = async key => await localforage.getItem(key).catch(readError)

export const storeOnDevice = async(key, value) => {
    // console.log('storeOnDevice', key, value)
    if (!isBrowser() || !key) return
    localforage.setItem(key, value).catch(error => {
        console.error(error)
        notifyError(`${key} could not be stored. You may use an unsupported device.`)
    })
}

export const getDefaultLanguage = (languages = availableUILanguages) => {
    if (typeof navigator === 'undefined') return 'en'
    let navigatorLang = navigator.language || navigator.userLanguage
    if (navigatorLang.length > 2) {
        navigatorLang = navigatorLang.substring(0, 2).toLowerCase()
    }
    const language = languages.find(l => l[0] === navigatorLang)
    const defaultLang = language && language[0] || 'en'
    return defaultLang
}

export const isOnboarded = async() => {
    const uiLanguage = await readFromDevice('uiLanguage')
    const bibleChoosen = await readFromDevice('onboard.bibleChoosen')
    return uiLanguage && bibleChoosen
}

export const send = async (url, body, type='POST', mimeType = 'text/json') => {
    const xhr = new XMLHttpRequest()
    xhr.open(type, url)
    xhr.overrideMimeType(mimeType)
    await new Promise((resolve) => {
        xhr.onload = resolve
        xhr.send(body)
    })
    return { status: xhr.status, response: xhr.response }
}
export const post = async (url, body) => send(url, body)

export const stats = async data => {
    const { navigator } = window
    data.language = navigator.language
    data.platform = navigator.platform
    data.userAgent = navigator.userAgent
    await post(`${DUOBIBLO_BACKEND}/api/stats`, JSON.stringify({ payload: data }))
}