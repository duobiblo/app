import bibleInfos from '../lib/bibleInfos.js'

export { bibleInfos }

export const DUOBIBLO_BACKEND = 'https://nehemia.duobiblo.com'
// export const DUOBIBLO_BACKEND = 'http://localhost:3060'

export const ICONS = {
    NO_BIBLES: '🙏',
    BOOKS: '📖',
    CHAPTERS: '🔢',
    SEARCH: '🔍',
}

export const DEFAULTS = {
    PROJECT_NAME: 'Duobiblo App',
    // DOWNLOAD_TRANSLATION_PATH: 'https://hmwrks.gitlab.io/vsk/',
    DOWNLOAD_TRANSLATION_PATH: 'https://duobiblo.gitlab.io/bdn/',
}

// export const bibles = {
//     // NIV: {label: 'New International Version', lang: 'en', size: 5239238},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
//     NVI: { label: 'Nueva Versión Internacional', lang: 'es', size: 5387063 },
//     HOF: { label: 'Hoffnung für Alle', lang: 'de', size: 5855612 },
//     'NVI-PT': { label: 'Nova Versão Internacional', lang: 'pt', size: 5318929 },
//     WEB: { label: 'World English Bible', lang: 'en', size: 5597548 },
//     'ERV-AR': { label: 'Arabic Bible: Easy-to-Read Version', lang: 'ar', size: 9363221 },
//     // ASV: {label: 'American Standard Version', lang: 'en', size: 5710540},
//     // KJV: {label: 'King James Version', lang: 'en', size: 5673603},
//     // DARBY: {label: 'Darby Translation', lang: 'en', size: 5639755},
//     // YLT: {label: 'Young\'s Literal Translation', lang: 'en', size: 5702628},
//     BDS: { label: 'La Bible du Semeur', lang: 'fr', size: 6450970 },

//     // 'NAV': 'Ketab El Hayat', // Arabic, العربية
//     // 'ERV-AWA': 'Awadhi Bible: Easy-to-Read Version', // Awadhi, अवधी
//     // 'BG1940': '1940 Bulgarian Bible', // Bulgarian, Български
// }
export const getBibleSize = abbr => bibleInfos[abbr].size
export const getBibleForLang = lang => Object.values(bibleInfos).find(value => value.lang === lang)
export const getBibleForCode = code => bibleInfos[code]
export const availableBibles = Object.values(bibleInfos)
    .sort((a, b) => a.languageOrigName.localeCompare(b.languageOrigName))

export const availableUILanguages = [
    [ 'en', 'English', 'English' ],
    [ 'es', 'Spanish', 'Español' ],
    [ 'de', 'German', 'Deutsch' ],
]


// currently used colors:
// 800080 - purple
// adff2f - limegreen 
export const colorSchemas = {
    mutedTones: { 
        color1: '#C0B283', /* pale gold */
        color2: '#DCD0C0', /* silk */
        color3: '#F4F4F4', /* paer */
        color4: '#373737', /* charcoal */
    },
    mutedMinimal: { 
        color1: '#96858F', /* dusty */
        color2: '#6D7993', /* lavendar */
        color3: '#9099A2', /* overcast */
        color4: '#D5D5D5', /* paper */
        color5: '#373737', /* charcoal */
    },
    boldAndPunch: { 
        color1: '#E24E42', /* papaya */
        color2: '#E9B000', /* mustard */
        color3: '#EB6E80', /* blush */
        color4: '#008F95', /* aqua */
    },
    summer: { 
        color1: '#286DA8', /* summer sky */
        color2: '#CD5360', /* warm peach */
        color3: '#B37D4E', /* field */
        color4: '#438496', /* breeze */
    },
    freshBlues: { 
        color1: '#77C9D4', /* feather */
        color2: '#57BC90', /* marine */
        color3: '#015249', /* forest */
        color4: '#A5A5AF', /* sleek grey */
    },
    unexpected: { 
        color1: '#6E3667', /* violet */
        color2: '#88D317', /* electrc lime */
        color3: '#1A0315', /* deep plum */
        color4: '#535353', /* shadow */
    },
    earthy: { 
        color1: '#945D60', /* terracota */
        color2: '#626E60', /* herb */
        color3: '#AF473C', /* chilli */
        color4: '#3C3C3C', /* charcoal */
    },
}

// name: no of chapters
export const books = {
    Genesis: 50,
    Exodus: 40,
    Leviticus: 27,
    Numbers: 36,
    Deuteronomy: 34,
    Joshua: 24,
    Judges: 21,
    Ruth: 4,
    '1 Samuel': 31,
    '2 Samuel': 24,
    '1 Kings': 22,
    '2 Kings': 25,
    '1 Chronicles': 29,
    '2 Chronicles': 36,
    Ezra: 10,
    Nehemia: 13,
    Esther: 10,
    Job: 42,
    Psalm: 150,
    Proverbs: 31,
    Ecclesiastes: 12,
    'Song of Songs': 8,
    Isaiah: 66,
    Jeremiah: 52,
    Lamentations: 5,
    Ezekiel: 48,
    Daniel: 12,
    Hosea: 14,
    Joel: 3,
    Amos: 9,
    Obadiah: 1,
    Jonah: 4,
    Micah: 7,
    Nahum: 3,
    Habakkuk: 3,
    Zephaniah: 3,
    Haggai: 2,
    Zechariah: 14,
    Malachi: 4,
    Matthew: 28,
    Mark: 16,
    Luke: 24,
    John: 21,
    Acts: 28,
    Romans: 16,
    '1 Corinthians': 16,
    '2 Corinthians': 13,
    Galatians: 6,
    Ephesians: 6,
    Philippians: 4,
    Colossians: 5,
    '1 Thessalonians': 5,
    '2 Thessalonians': 3,
    '1 Timothy': 6,
    '2 Timothy': 4,
    Titus: 3,
    Philemon: 1,
    Hebrews: 13,
    James: 5,
    '1 Peter': 5,
    '2 Peter': 3,
    '1 John': 5,
    '2 John': 1,
    '3 John': 1,
    Jude: 1,
    Revelation: 22,
}
export const bookList = Object.keys(books)

// Abbreviations for display
export const bookAbbreviations = {
    Genesis: 'Gen',
    Exodus: 'Ex',
    Leviticus: 'Lev',
    Numbers: 'Num',
    Deuteronomy: 'Dt',
    Joshua: 'Jo',
    Judges: 'Jud',
    Ruth: 'Rt',
    '1 Samuel': '1Sa',
    '2 Samuel': '2Sa',
    '1 Kings': '1Ki',
    '2 Kings': '2Ki',
    '1 Chronicles': '1Ch',
    '2 Chronicles': '2Ch',
    Ezra: 'Ez',
    Nehemia: 'Nh',
    Esther: 'Est',
    Job: 'Job',
    Psalm: 'Ps',
    Proverbs: 'Pr',
    Ecclesiastes: 'Ecc',
    'Song of Songs': 'Sng',
    Isaiah: 'Isa',
    Jeremiah: 'Jer',
    Lamentations: 'Lam',
    Ezekiel: 'Eze',
    Daniel: 'Da',
    Hosea: 'Ho',
    Joel: 'Jo',
    Amos: 'Am',
    Obadiah: 'Ob',
    Jonah: 'Jon',
    Micah: 'Mic',
    Nahum: 'Na',
    Habakkuk: 'Hb',
    Zephaniah: 'Ze',
    Haggai: 'Hg',
    Zechariah: 'Za',
    Malachi: 'Mal',
    Matthew: 'Mt',
    Mark: 'Mk',
    Luke: 'Luk',
    John: 'Jn',
    Acts: 'Act',
    Romans: 'Rom',
    '1 Corinthians': '1Co',
    '2 Corinthians': '2Co',
    Galatians: 'Gal',
    Ephesians: 'Eph',
    Philippians: 'Ph',
    Colossians: 'Col',
    '1 Thessalonians': '1Th',
    '2 Thessalonians': '2Th',
    '1 Timothy': '1Ti',
    '2 Timothy': '2Ti',
    Titus: 'Tit',
    Philemon: 'Phi',
    Hebrews: 'Heb',
    James: 'Jam',
    '1 Peter': '1Pt',
    '2 Peter': '2Pt',
    '1 John': '1Jo',
    '2 John': '2Jo',
    '3 John': '3Jo',
    Jude: 'Jud',
    Revelation: 'Rev',
}

// Synonyms for search
export const bookSynonyms = {
    gn: 'Genesis',
    ex: 'Exodus',
    lv: 'Leviticus',
    nm: 'Numbers',
    dt: 'Deuteronomy',
    jo: 'Joshua',
    jg: 'Judges',
    rt: 'Ruth',
    '1sa': '1 Samuel',
    '2sa': '2 Samuel',
    '1ki': '1 Kings',
    '2ki': '2 Kings',
    '1ch': '1 Chronicles',
    '2ch': '2 Chronicles',
    ezr: 'Ezra',
    nh: 'Nehemia',
    est: 'Esther',
    jb: 'Job',
    ps: 'Psalm',
    pr: 'Proverbs',
    ecc: 'Ecclesiastes',
    sng: 'Song of Songs',
    isa: 'Isaiah',
    jr: 'Jeremiah',
    lm: 'Lamentations',
    ez: 'Ezekiel',
    da: 'Daniel',
    ho: 'Hosea',
    joe: 'Joel',
    am: 'Amos',
    ob: 'Obadiah',
    jh: 'Jonah',
    mc: 'Micah',
    na: 'Nahum',
    hb: 'Habakkuk',
    ze: 'Zephaniah',
    hg: 'Haggai',
    zec: 'Zechariah',
    mal: 'Malachi',
    mt: 'Matthew',
    mk: 'Mark',
    lk: 'Luke',
    jn: 'John',
    act: 'Acts',
    rm: 'Romans',
    '1co': '1 Corinthians',
    '2co': '2 Corinthians',
    gl: 'Galatians',
    eph: 'Ephesians',
    ph: 'Philippians',
    cl: 'Colossians',
    '1th': '1 Thessalonians',
    '2th': '2 Thessalonians',
    '1ti': '1 Timothy',
    '2ti': '2 Timothy',
    tt: 'Titus',
    pl: 'Philemon',
    heb: 'Hebrews',
    jm: 'James',
    '1pt': '1 Peter',
    '2pt': '2 Peter',
    '1jo': '1 John',
    '2jo': '2 John',
    '3jo': '2 John',
    jd: 'Jude',
    rv: 'Revelation',
}