import { goto } from '@sapper/app'
import { get } from 'svelte/store'
import { DEFAULTS, getBibleSize } from '../lib/params.js'
import { notifyImmediate, clearNotification, notifyError } from '../stores/NotificationStore.js'
import { translations, currentBibles } from '../stores/BibleStore.js'
import { storeOnDevice, stats } from '../lib/helpers.js'
import { t } from '../i18n/i18n.js'

const getProgressCallback = (translation, contentLength) => async percent => {
    let currrentPercentage = Math.min(Math.floor((percent/contentLength)*100, 2), 100)
    notifyImmediate({ msg: `${t('Downloading')} ${translation} (${currrentPercentage}%)` })
}

export const downloadTranslations = async (translationsToFetch) => {
    stats({ translationsToFetch })
    const fetchedTranslations = {}
    const $translations = get(translations)
    const existingTranslationKeys = ($translations && Object.keys($translations)) || []
    translationsToFetch = existingTranslationKeys 
        ? translationsToFetch.filter(t => !existingTranslationKeys.includes(t)) 
        : translationsToFetch

    for (let translation of translationsToFetch) {
        const contentLength = getBibleSize(translation)
        const progressCallback = getProgressCallback(translation, contentLength)
        const url = `${DEFAULTS.DOWNLOAD_TRANSLATION_PATH}${translation}.js`
        fetchedTranslations[translation] = await fetchJSON(url, progressCallback)
    }
    Object.assign(fetchedTranslations, $translations)
    translations.set(fetchedTranslations)
    
    // finishFetchTranslations(fetchedTranslations)
    // if no current bible is selected yet preselect the first one downloaded
    const $currentBibles = get(currentBibles)
    if ((!$currentBibles || $currentBibles.length < 1) && (Object.keys(fetchedTranslations).length)) {
        currentBibles.set([ Object.keys(fetchedTranslations)[0] ])
    }

    clearNotification()
    if (existingTranslationKeys.length || translationsToFetch.length) {
        storeOnDevice('onboard.bibleChoosen', true)
    }

    goto('/')
}

export const fetchJSON = async(url, progressCallback) => {
    const response = await fetch(url)

    if (response.status !== 200) {
        clearNotification()
        notifyError(`The download of ${url} failed, please try later (${response.status})`)
        throw response
    }

    const reader = response.body.getReader()

    progressCallback && await progressCallback(0)

    // read the data
    let receivedLength = 0 // received that many bytes at the moment
    let chunks = [] // array of received binary chunks (comprises the body)
    while(true) {
        const { done, value } = await reader.read()

        if (done) {
            break
        }

        chunks.push(value)
        receivedLength += value.length

        progressCallback && await progressCallback(receivedLength)
    }

    // concatenate chunks into single Uint8Array
    let chunksAll = new Uint8Array(receivedLength)
    let position = 0
    for(let chunk of chunks) {
        chunksAll.set(chunk, position)
        position += chunk.length
    }

    // decode into a string
    const result = new TextDecoder('utf-8').decode(chunksAll)

    // parse as json
    return JSON.parse(result)
}
