
import { writable } from 'svelte/store'
import { isBrowser, readFromDevice, storeOnDevice } from '../lib/helpers.js'

export const uiLanguage = writable()

const loadFromDevice = async() => {
    if (isBrowser()) {
        const $uiLanguage = await readFromDevice('uiLanguage')
        // uiLanguage.set($uiLanguage || 'en')
        $uiLanguage && uiLanguage.set($uiLanguage)
    }
}
loadFromDevice()

uiLanguage.subscribe(value => isBrowser() && value && storeOnDevice('uiLanguage', value))
