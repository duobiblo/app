import { writable, get, derived } from 'svelte/store'
import { books, bookList, bookSynonyms, ICONS } from '../lib/params.js'
import { rangeFromTo, storeOnDevice, readFromDevice, isBrowser } from '../lib/helpers.js'
import { notify } from './NotificationStore.js'
import { t } from '../i18n/i18n.js'

export const translations = writable({})
export const currentBibles = writable()
export const currentBook = writable('')
export const currentChapter = writable('')
export const verses = writable([]) // displayed in Chapter.svelte
export const searchResult = writable([]) // search result contains book/chapter headers and verses
export const fullTextSearchResultCount = writable(0)
export const searchElement = writable()
export const isSearching = writable(false)
export const isLoaded = writable(false)
export const markedVerses = writable([])

const loadFromDevice = async() => {
    if (isBrowser()) {
        const $currentBibles = await readFromDevice('currentBibles')
        // console.log('loadCurrentBibles', $currentBibles)
        const v = $currentBibles && $currentBibles.length ? $currentBibles : undefined
        currentBibles.set(v)

        const $translations = await readFromDevice('translations')
        // console.log('loadTranslations', $translations)
        translations.set($translations)
        // console.log('length', $translations && Object.keys($translations).length)
        if (!$translations || !Object.keys($translations).length) {
            currentBibles.set([])
        }

        const $currentBook = await readFromDevice('currentBook')
        // console.log('$currentBook', $currentBook)
        currentBook.set($currentBook || 'John')

        const $currentChapter = await readFromDevice('currentChapter')
        // console.log('$currentChapter', $currentChapter)
        currentChapter.set($currentChapter || 1)
        isLoaded.set(true)
    }
}
loadFromDevice()

translations.subscribe(value => isBrowser() && value && Object.keys(value).length && storeOnDevice('translations', value))
currentBibles.subscribe(value => isBrowser() && value && storeOnDevice('currentBibles', value))
currentBook.subscribe(value => isBrowser() && value && storeOnDevice('currentBook', value))
currentChapter.subscribe(value => isBrowser() && value && storeOnDevice('currentChapter', value))

currentBook.subscribe(() => markedVerses.set({}))
// Somehow this subscription sets the value of currentChapter to an empty string
currentChapter.subscribe(() => markedVerses.set([]))


export const nextChapter = () => {
    // markedVerses.set({})
    const bookLength = books[get(currentBook)]
    const currentChapterValue = get(currentChapter)
    // console.log('nextChapter', get(currentBook), bookLength, get(verses).length, currentChapterValue)
    if (bookLength > currentChapterValue) {
        currentChapter.set(currentChapterValue + 1)
    } else {
        const index = bookList.indexOf(get(currentBook))
        let newBookName = bookList[0]
        if (index < bookList.length - 1) {
            newBookName = bookList[index + 1]
        }
        // console.log('newBookName', newBookName)
        currentBook.set(newBookName)
        currentChapter.set(1)
    }
    window.scrollTo({ top: 0, behavior: 'auto' })
}

export const previousChapter = () => {
    // markedVerses.set({})
    const currentChapterValue = get(currentChapter)
    if (currentChapterValue > 1) {
        currentChapter.set(currentChapterValue - 1)
    } else {
        const index = bookList.indexOf(get(currentBook))
        let newBookName = bookList[bookList.length - 1]
        if (index > 0) {
            newBookName = bookList[index - 1]
        }
        currentBook.set(newBookName)
        currentChapter.set(books[newBookName])
    }
    window.scrollTo({ top: 0, behavior: 'auto' })
}

translations.subscribe($translations => {
    // console.log('translations subscribtion triggered')
    currentBibles.subscribe($currentBibles => {
        // console.log('currentBibles subscribtion triggered')
        markedVerses.subscribe(() => {
            currentBook.subscribe($currentBook => {
            // console.log('currentBook subscribtion triggered')
                currentChapter.subscribe($currentChapter => {
                    // console.log('currentChapter subscribtion triggered', $currentChapter)
                    // console.log('translations.length', Object.keys($translations).length)
                    if ($currentBibles && $currentBook && $translations && Object.keys($translations).length) {
                        // console.log('currentBook', $currentBibles, $currentBook, $translations['NIV']['Genesis'], $currentChapter)
                        if ($currentBook !== ICONS.BOOKS && $currentChapter !== ICONS.CHAPTERS) {
                            // console.log('$currentBibles', $currentBibles, $currentBook, $currentChapter)
                            const chapters = $currentBibles
                                .map((currentBible) => {
                                    // console.log('currentBible', currentBible, $translations[currentBible][$currentBook][$currentChapter])
                                    const chapter = $translations[currentBible] && $translations[currentBible][$currentBook] && $translations[currentBible][$currentBook][$currentChapter]
                                    // console.log('chapter', chapter)
                                    if (chapter) chapter[0].n = 1
                                    return chapter
                                })
                                .filter(c => c)
                            // console.log(chapters.length, chapters)
                            verses.set(chapters)
                        } else {
                            search(get(searchElement))
                        }
                    }
                })
            })
        })
    })
})

// unfortunately the derived callback is not triggered when `set` is called on a dependent store
// derived(translations, [ currentBibles, currentBook, currentChapter ],
//     ($translations, [ $currentBibles, $currentBook, $currentChapter ], set) => {
//         console.log('verses2', $translations, $currentBibles, $currentBook, $currentChapter)
//         // export const verses = derived(translations, currentBibles,
//         //     (a, b, set) => {
//         // console.log('derived', a)
//         console.log('derived', translations, currentBibles, currentBook, currentChapter, set)
//             // console.log('currentBibles', currentBibles)
//         if (currentBibles && currentBook && Object.keys(translations).length) {
//             console.log('currentBook', currentBook, Object.keys(translations).length)
//             set([])
//             const chapters = currentBibles.map((currentBible) =>
//                 translations[currentBible][currentBook][currentChapter])
//             if (chapters.length) {
//                 // verses = range(chapters[0].length).map(i => range(chapters.length).map(j => chapters[j][i]))
//                 set(range(chapters[0].length).map(i => range(chapters.length).map(j => chapters[j][i])))
//             } else {
//                 set([])
//                     // verses = []
//             }
        // }
    // })

const SEARCH_TERM_DELIMITER = ';'

export const clearLastSearch = () => {
    if (get(searchElement)) {
        get(searchElement).value = ''
    }
    currentChapter.set(1)
    fullTextSearchResultCount.set(0)
    isSearching.set(false)
}

// Search for verse ranges or full text
export const search = async(element) => {
    if (!element || !element.value) return
    let searchString = element.value
    // console.log('searchString', searchString)
    if (!searchString || searchString.length < 3) return notify({ msg: t('You need to enter at least 3 characters') })
    searchElement.set(element)
    if (get(isSearching)) return
    isSearching.set(true)
    // currentBook.set(ICONS.BOOKS)
    // currentChapter.set(ICONS.CHAPTERS)
    fullTextSearchResultCount.set(0)
    // verses.set([])
    if (isVerseSearch(searchString)) {
        const terms = searchString.split(SEARCH_TERM_DELIMITER)
        const searchResults = terms.map(searchTerm).flat()
        if (searchResults && searchResults[0] !== undefined) {
            // verses.set(searchResults)
            searchResult.set(searchResults.filter(r => r))
        }
    } else {
        fulltextSearch(searchString)
    }
    // await sleep(2000)
    isSearching.set(false)
}


const isVerseSearch = term => (/^\d?[A-Za-z]{2,}\d/).test(term.replace(/\s/g, ''))
const REGEXP_WHITESPACE = /\s/g

const searchTerm = term => {
    term = term.toLowerCase().replace(REGEXP_WHITESPACE, '')
    const indexOfFirstNumber = term.slice(1).search(/\d/)
    const synonymToSearch = indexOfFirstNumber === -1 ?
        term :
        term.substring(0, indexOfFirstNumber + 1)
    let book = bookSynonyms[synonymToSearch]
    if (!book) {
        book = bookList.find(b => b.toLowerCase().replace(REGEXP_WHITESPACE, '').startsWith(synonymToSearch))
    }
    const chapterAndVerses = indexOfFirstNumber > -1 && term.substring(indexOfFirstNumber + 1).split(/[:]/)
    if (book && chapterAndVerses && chapterAndVerses.length < 2) { // no verse specified, just select book and chapter
        currentBook.set(book)
        currentChapter.set(parseInt(chapterAndVerses[0], 10))
        get(searchElement).value = ''
        return
    }
    const $translations = get(translations)
    const $currentBibles = get(currentBibles)
    if (!$currentBibles || !$currentBibles.length) return
    const chapter = chapterAndVerses && chapterAndVerses[0]
    const firstCurrentBible = $currentBibles[0]
    if (!$translations[firstCurrentBible][book] || !$translations[firstCurrentBible][book][chapter]) return
    const maxTo = $translations[firstCurrentBible][book][chapter].length
    const verseArray = chapterAndVerses && chapterAndVerses.length > 1 ?
        chapterAndVerses.slice(1)[0].split(',').map(v => {
            if (v.indexOf('-') > -1) {
                let [ from, to ] = v.split('-')
                to = Math.min(to, maxTo)
                from = Math.max(from, 1)
                from = Math.min(from, to)
                return rangeFromTo(from, to)
            }
            return parseInt(v, 10)
        }).flat() : rangeFromTo(1, maxTo)
    const res = verseArray.map(n => {
        const rawVerses = $currentBibles.map(bible => {
            // console.log(bible, book, chapter, n, $translations[bible][book][chapter])
            const a = $translations[bible][book][chapter].find(v => {
                // console.log({ vn: v.n, n, test: v.n === n, }, typeof v.n, typeof n)
                return v.n === n
            })
            // if (a) console.log({ chapter, n, a, book })
            if (a) a.book = book
            if (a) a.chapter = chapter
            return a
        })
        // console.log(v2)
        return rawVerses
    }).filter(r => r[0])
    // console.log(res)
    let lastBook
    return res.reduce((memo, item) => {
        // console.log({ memo, item })
        // console.log(lastBook, item[0].book, item)
        if (lastBook !== item[0].book) {
            // console.log('push', item[0].book)
            memo.push({ book: item[0].book, chapter: item[0].chapter })
            // memo.push([ { book: item[0].book, chapter: item[0].chapter } ])
            lastBook = item[0].book
        }
        memo.push(item)
        return memo
    }, [])
}

const fulltextSearch = (searchString) => {
    searchString = searchString.toLowerCase()
    const $translations = get(translations)
    const $currentBibles = get(currentBibles)
    if (!$currentBibles || !$currentBibles.length) return
    const firstTranslations = $currentBibles[0]
    const newVerses = []
    let found = 0
    let lastBook
    let lastChapter = $currentBibles
    const regexp = new RegExp(`(${searchString})`, 'gi')
    Object.entries($translations[firstTranslations]).forEach(([ book, chapters ]) => {
        // console.log('tranlations.t', { book, chapters })
        Object.entries(chapters).forEach(([ chapter, verses ]) => {
            // console.log('chapters', chapter, verses)
            verses.forEach(({ n, t }) => {
                // console.log('verse', { n, t })
                const offset = t && t.toLowerCase().indexOf(searchString)
                if (offset > -1) {
                    if (lastBook !== book || lastChapter !== chapter) {
                        // console.log('found', { n, t, book, lastBook })
                        newVerses.push({ book, chapter })
                        lastBook = book
                        lastChapter = chapter
                    }
                    // console.log('found', { n, t, book, lastBook })
                    // const newT = t.slice(0, offset)
                    const newT = t.replace(regexp, '<found>$1</found>')
                    found++
                    // console.log(searchString, t)
                    // console.log(newT)
                    newVerses.push([ { n, t: newT } ])
                }
            })
        })
    })
    // verses.set(newVerses)
    searchResult.set(newVerses)
    fullTextSearchResultCount.set(found)
}