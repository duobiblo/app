import { writable, get } from 'svelte/store'

export const current = writable({})
let queue = []
let timeoutID

// example: notify({msg: 'You're good'})
export const notify = (args) => {
    queue.push(args)
    go()
}
export const notifyError = msg => notify(Object.assign({ msg }, { type: 'error' }))
  
// after `notifyImmediate` you have to call `clearNotification` otherwise `notify` won't work anymore
// example: notifyImmediate({msg: 'Downloading...', percent: 30})
export const notifyImmediate = (args) => {
    if (timeoutID) clearTimeout(timeoutID)
    current.set(args)
}

export const clearNotification = () => {
    current.set({})
}

export const info = msg => notify({ msg })

const nextGo = () => {
    current.set({})
    go()
}

const go = () => {
    // console.log('in go', get(current), get(current).msg)
    if (get(current).msg) return
    // const item = queue.pop()
    const item = queue.shift()
    // console.log('popped', item)
    if (item) {
        current.set(item)
        timeoutID = setTimeout(nextGo, 5000)
    }
}