import { writable } from 'svelte/store'
import { storeOnDevice, readFromDevice, isBrowser } from '../lib/helpers.js'

export const isSwipe = writable(false)

const loadFromDevice = async() => {
    if (isBrowser()) {
        const $isSwipe = await readFromDevice('settings.isSwipe')
        // console.log('loadFromDevice', $isSwipe)
        isSwipe.set($isSwipe)
    }
}
loadFromDevice()

isSwipe.subscribe(value => storeOnDevice('settings.isSwipe', value))
