<div>
    <p>{t('UILanguageChooser.welcome', { projectName: DEFAULTS.PROJECT_NAME })}</p>

    <h2>{t('What is your primary language?')}</h2>

    <select class="voices" bind:value={currentLanguage} title="{t('Voice')}">
        {#each availableUILanguages as language}
            <option key={language[0]} value={language[0]}>{language[1]} ({language[2]})</option>
        {/each}
    </select>
    <button on:click={onLanguageChoosen}>{t('Done')}</button>
</div>

<script>
import { onMount } from 'svelte'
import { get } from 'svelte/store'
import { goto } from '@sapper/app'
import { DEFAULTS, availableUILanguages } from '../lib/params.js'
import { getDefaultLanguage } from '../lib/helpers.js'
import { uiLanguage } from '../stores/UILanguage.js'
import { t } from '../i18n/i18n.js'

let currentLanguage
export let returnAddress = "/"

onMount(async () => {
    const defaultLang = getDefaultLanguage(availableUILanguages)
    currentLanguage = get(uiLanguage) || defaultLang
})

const onLanguageChoosen = () => {
    uiLanguage.set(currentLanguage)
    goto(returnAddress)
}

</script>

<style>
    div {
        display: flex;
        flex-direction: column;
        text-align: center;
        max-width: 500px;
        margin: auto;
    }
</style>