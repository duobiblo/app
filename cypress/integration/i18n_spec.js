// import { describe } from 'cypress'
// import { DEFAULTS } from '../../../src/lib/params.js'
import { t } from '../../src/i18n/i18n.js'
import { uiLanguage } from '../../src/stores/UILanguage.js'

// TODO: this should be a unit test
describe('i18n', () => {
    beforeEach(() => {
        // cy.visit('/')
    })

    it('get a simple default (English) translation', () => {
        // cy.contains('p', `With ${DEFAULTS.PROJECT_NAME} you can practice your language skills studying books of the bible in different languages.`)
        // Rate: 'Velocidad',
        uiLanguage.set('en')
        assert.equal(t('Rate'), 'Rate')
    })

    it('get simple Spanish value', () => {
        uiLanguage.set('es')
        assert.equal(t('Rate'), 'Velocidad')
    })

    it('get Spanish value dot', () => {
        uiLanguage.set('es')
        assert.equal(t('onboard.info2.1'), 'Si prefieres otra traducción puedes ir a')
    })

    it('get Spanish nested value', () => {
        uiLanguage.set('es')
        assert.equal(t('test.levelOne'), 'Nivel Uno')
        assert.equal(t('test.levelTwo.levelOne'), 'Nivel Uno dentro de Nivel Dos')
    })
})