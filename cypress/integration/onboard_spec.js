import { DEFAULTS } from '../../src/lib/params.js'
import { uiLanguage } from '../../src/stores/UILanguage.js'

describe('Onboarding', () => {
    beforeEach(() => {
        uiLanguage.set('en')
        cy.visit('/')
    })

    it('We are redirected to onboarding page 1 (UILanguageChooser)', () => {
        cy.url().should('include', '/onboard')
        cy.contains('p', `With ${DEFAULTS.PROJECT_NAME} you can practice your language skills studying books of the bible in different languages.`)
    })

    // it('navigates to /about', () => {
    //     cy.get('nav a').contains('about').click()
    //     cy.url().should('include', '/about')
    // })

    // it('navigates to /blog', () => {
    //     cy.get('nav a').contains('blog').click()
    //     cy.url().should('include', '/blog')
    // })
})